package main

import "fmt"

type Student struct {
	name  string
	age   int
	score float64
}

func showStudent(student Student) {
	fmt.Println("Name:", student.name)
	fmt.Println("Age:", student.age)
	fmt.Println("Score:", student.score)
}

func main() {
	var s Student

	s.name = "Preechanon Chatthai"
	s.age = 21
	s.score = 99.5

	showStudent(s)
}
