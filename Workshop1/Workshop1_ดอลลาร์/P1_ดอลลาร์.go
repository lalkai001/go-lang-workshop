package main

import "fmt"

func average(numbers []float64) float64 {
	total := 0.0
	for _, num := range numbers {
		total += num
	}
	return total / float64(len(numbers))
}
func main() {
	numbers := []float64{1.5, 2.0, 3.5, 4.0, 5.5, 6.0, 7.5}
	avg := average(numbers)
	fmt.Println(avg)
}
