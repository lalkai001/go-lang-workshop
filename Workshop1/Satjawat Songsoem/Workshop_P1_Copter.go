package main
import "fmt"

var sum, avg float64
func sumNumber(list_number []float64) float64 {
	for i:=0; i<len(list_number); i++{
		sum += list_number[i]
	} 
	return sum
}

func main() {
	list_number := []float64{1.5,2.0,3.5,4.0,5.5,6.0,7.5}
	avg := sumNumber(list_number) / float64(len(list_number))
	fmt.Println(avg)
}